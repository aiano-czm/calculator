#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QWindow>

QString displayText("0");
double num[2] = {0};
bool operatorFlag = false;
bool resultFlag = false;
QString operatorStr("");

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->Display->setText(displayText); // 显示初始内容
    setFixedSize(350, 450); // 固定窗口大小
    this->setWindowIcon(QIcon(":/icon.ico")); // 设置主窗口图标（左上角）
                                              // 注意：需要先在Qt工程里添加资源文件才能引用


    // 绑定按键回调函数
    QPushButton *numButton[10]; // 数字按键
    for(int i=0;i<10;i++){
        QString buttonName = "Button_" + QString::number(i);
        numButton[i] = MainWindow::findChild<QPushButton *>(buttonName);
        connect(numButton[i],SIGNAL(released()),this,SLOT(NumButtonReleasedCallback()));
    }
    connect(ui->Button_plus,SIGNAL(released()),this,SLOT(OperatorButtonReleasedCallback())); // 加
    connect(ui->Button_substract,SIGNAL(released()),this,SLOT(OperatorButtonReleasedCallback())); // 减
    connect(ui->Button_multiply,SIGNAL(released()),this,SLOT(OperatorButtonReleasedCallback())); // 乘
    connect(ui->Button_divide,SIGNAL(released()),this,SLOT(OperatorButtonReleasedCallback())); // 除
    connect(ui->Button_equal,SIGNAL(released()),this,SLOT(EqualButtonReleasedCallback())); // 等于
    connect(ui->Button_sign,SIGNAL(released()),this,SLOT(SignButtonReleasedCallback())); // 符号
    connect(ui->Button_clear,SIGNAL(released()),this,SLOT(ClearButtonReleasedCallback())); // 清除
    connect(ui->Button_clear_error,SIGNAL(released()),this,SLOT(ClearErrorButtonReleasedCallback())); // 清除错误
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::NumButtonReleasedCallback()
{
    QPushButton *button = (QPushButton *)sender(); // 获取发出信号按键的指针
    QString buttonText = button->text(); // 获取按键字符串
    qDebug() << buttonText;

    if(displayText == "0" || operatorFlag || resultFlag) // 输入数字
    {
        // 处理标识符
        operatorFlag = false;
        resultFlag = false;

        displayText = buttonText;
    }else{
        displayText += buttonText;
    }

    if(operatorStr == "") // 记录数字
    {
        num[0] = displayText.toDouble();
    }else{
        num[1] = displayText.toDouble();
    }

    ui->Display->setText(displayText); // 显示新数
}

void MainWindow::OperatorButtonReleasedCallback()
{
    operatorFlag = true; // 处理标志位
    // 记录操作符
    QPushButton *button = (QPushButton *)sender(); // 获取发出信号按键的指针
    QString buttonText = button->text(); // 获取按键字符串
    operatorStr = buttonText;
    qDebug() << operatorStr;
}

void MainWindow::EqualButtonReleasedCallback()
{
    if(!resultFlag){
        double result = 0;
        if(operatorStr == "+"){
            result = num[0] + num[1];
        }else if(operatorStr == "-"){
            result = num[0] - num[1];
        }else if(operatorStr == "×"){
            result = num[0] * num[1];
        }else if(operatorStr == "÷"){
            result = num[0] / num[1];
        }else{
            result = num[0];
        }
        resultFlag = true; // 处理标志位

        ui->Display->setText(QString::number(result)); // 显示新数

        /* 小彩蛋 */
        if(result == 328){
            ui->Display->setText(QString("Mo"));
        }else if(result == 623){
            ui->Display->setText(QString("ZaZ"));
        }
        if(operatorStr == "+"){
            if(num[0] == 328 && num[1] == 623){
                ui->Display->setText(QString("Mo❤ZaZ"));
            }else if(num[0] == 623 && num[1] == 328){
                ui->Display->setText(QString("ZaZ❤Mo"));
            }
        }
        /* 小彩蛋 */

        operatorStr = "";
        num[0] = result;
    }
}

void MainWindow::SignButtonReleasedCallback()
{
    if(operatorStr == "") // 记录数字
    {
        num[0] = -num[0];
        ui->Display->setText(QString::number(num[0])); // 显示新数
    }else{
        num[1] = -num[1];
        ui->Display->setText(QString::number(num[1])); // 显示新数
    }
}

void MainWindow::ClearButtonReleasedCallback()
{
    num[0] = 0;
    num[1] = 0;
    operatorFlag = false;
    resultFlag = false;
    displayText = "0";
    ui->Display->setText(displayText); // 显示新数
}

void MainWindow::ClearErrorButtonReleasedCallback()
{
    if(operatorStr == "") // 记录数字
    {
        num[0] = 0;
    }else{
        num[1] = 0;
    }
    displayText = "0";
    ui->Display->setText(displayText); // 显示新数
}
