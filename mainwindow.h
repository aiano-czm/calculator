#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void NumButtonReleasedCallback();
    void OperatorButtonReleasedCallback();
    void EqualButtonReleasedCallback();
    void SignButtonReleasedCallback();
    void ClearButtonReleasedCallback();
    void ClearErrorButtonReleasedCallback();
};
#endif // MAINWINDOW_H
